package org.logger

import groovy.time.TimeCategory
import java.text.SimpleDateFormat
 
// Use annotation to inject log field into the class.
class LoggerClass {

    def formatMessage(logLevel, message) {
        def date = new Date()
        def sdf = new SimpleDateFormat("dd-MM-y HH:mm:ss.SSS")

        return "[${sdf.format(date)}][${logLevel}][dev][jenkins][DLMT Type][Version]: ${message}"
    }

    def info(message) {
        def formattedMessage = this.formatMessage('INFO', message)
        echo formattedMessage
    }

    def warning(message) {
        def formattedMessage = this.formatMessage('WARNING', message)
        echo formattedMessage
    }

    def debug(message) {
        def formattedMessage = this.formatMessage('DEBUG', message)
        echo formattedMessage
    }

    def fatal(message) {
        def formattedMessage = this.formatMessage('FATAL', message)
        echo formattedMessage
    }
}
 