def call(Closure body) {
    node('_steps') {
        logstash{
            body()
        }
    }
}